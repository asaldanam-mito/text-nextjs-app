import { useCallback, useMemo, useRef } from "react";
import useWebSocket, { ReadyState } from "react-use-websocket";

export default function useTestWebsocket() {
  //Public API that will echo messages sent to it back to the client
  const socketUrl = process.env.NEXT_PUBLIC_WSS;
  if (!socketUrl) {
    console.error(`No se ha encontrado la clave NEXT_PUBLIC_WSS en el .env`);
  }

  const messageHistory = useRef([]);
  const { sendMessage, lastMessage, readyState } = useWebSocket(socketUrl, {
    onOpen: () => {
      // Mensaje de inicio
      const msg = {
        type: "subscribe",
        channel: "marketdata",
        interval: 500,
        market_id: "BTC-USDT",
        filter: ["ticker", "order_books"],
      };
      sendMessage(JSON.stringify(msg));
    },
  });

  messageHistory.current = useMemo(
    () => messageHistory.current.concat(lastMessage),
    [lastMessage]
  );

  const send = useCallback(() => {
    sendMessage(JSON.stringify(msg));
  }, []);

  const connectionStatus = {
    [ReadyState.CONNECTING]: "Connecting",
    [ReadyState.OPEN]: "Open",
    [ReadyState.CLOSING]: "Closing",
    [ReadyState.CLOSED]: "Closed",
    [ReadyState.UNINSTANTIATED]: "Uninstantiated",
  }[readyState];

  const websocket = {
    connectionStatus,
    readyState,
    lastMessage,
    messageHistory,
  };

  return websocket || {};
}
