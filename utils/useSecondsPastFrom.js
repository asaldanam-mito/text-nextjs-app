import { useEffect, useState } from "react";

export default function useSecondsPastFrom(date) {
  const [secondsPast, setSecondsPast] = useState();
  const datetime = new Date(date).getTime(); //ms
  
  useEffect(() => {
    setInterval(() => {
      const now = new Date().getTime(); //ms
      const miliseconds = now - datetime;
      const seconds = Math.floor(miliseconds / 1000);
      setSecondsPast(seconds);
    }, 1000)
  }, [date])

  return {secondsPast}
}