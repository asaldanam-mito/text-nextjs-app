import Head from "next/head";
import { useEffect, useState } from "react";
import { ReadyState } from "react-use-websocket";
import useTestWebsocket from "../hooks/useTestWebsocket";
import styles from "../styles/Home.module.css";

const Test = () => {
  // const [value, setValue] = useState("");
  const { connectionStatus, send, messageHistory, readyState } =
    useTestWebsocket();

  const messages = messageHistory?.current
    .map((message, idx) => {
      const data = JSON.parse(message?.data || "{}");
      return data;
    })
    .filter((nulls) => nulls);

  console.log({ messages });

  return (
    <main className={styles.main}>
      <p className={styles.description}>Websocket demo</p>
      <p>The WebSocket is currently {connectionStatus}</p>
      <div></div>
    </main>
  );
};

export default function Page({}) {
  const [isBrowser, setIsBrowser] = useState(false);
  useEffect(() => setIsBrowser(true), []);

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {isBrowser && <Test />}

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{" "}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  );
}

export async function getStaticProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  };
}
