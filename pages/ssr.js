import Head from 'next/head'
import styles from '../styles/Home.module.css'
import useSecondsPastFrom from '../utils/useSecondsPastFrom'

export default function SSR({ datetime }) {
  const {secondsPast} = useSecondsPastFrom(datetime)

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Server side fetch on each request
        </h1>

        <p className={styles.description}>
          Data updated <code className={styles.code}>{secondsPast}</code> seconds ago
        </p>

        <p className={styles.description}>
          <code className={styles.code}>{JSON.stringify({datetime})}</code>
        </p>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}

export async function getServerSideProps(context) {
  const res = await fetch(`https://worldtimeapi.org/api/timezone/Europe/Madrid`)
  const data = await res.json()

  if (!data) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      datetime: data.datetime,
    }, // will be passed to the page component as props
  }
}